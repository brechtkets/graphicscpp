# C++ 2D Graphics Library Proposal: Implementations

## Style and Formatting

If you wish to make changes to the library API, please familiarize yourself with 17.5 \[description\] from the ISO C++ Standard (the "Standard") (you can obtain the most recent working draft from [isocpp.org][1]) and with the guidance on the [Call for Library Proposals][1] webpage. Doing our best to keep to the style requirements and guidelines is important, though minor errors will almost certainly creep in at times and be periodically purged.

As far as indent style, if you are starting your own implementation, feel free to use whichever style you prefer. I personally chose 1TBS (with else on a new line) though I also often write code in Allman style. There are benefits (and detriments) to any style, of course. More important than which style someone chooses is the need to maintain consistency. If you decide to work from an existing implementation, please do not start out by changing the formatting and please keep your additions and modifications stylistically consistent with the formatting that the original author chose. Sorry to repeat a bunch of things I'm sure you know already.

[1]: http://www.isocpp.org/
[2]: http://open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3370.html

## Basics of the Library

The core library API consists of the following classes and typedefs:

	namespace std_graphics_2d_ts {
		class pixel_format;
		class no_graphics_2d_support;
		class window;
		
		template <class Traits>
		class graphics_traits_2d;
		
		template <class GraphicsTraits>
		class graphics_device_2d;
		
		template <class GraphicsTraits>
		class graphics_context_2d;
		
		template <class GraphicsTraits>
		class font_object;
		
		template <class GraphicsTraits>
		class texture_2d;
		
		template <class GraphicsTraits, class BrushType>
		class brush;
		
		struct vector_2;
		
		struct rect;
		
		struct rgba_unorm_color;
		
		// unknown is a class type that can be used as a template argument to graphics_traits_2d.
		typedef unknown default_graphics_traits_2d; 
		
		typedef graphics_traits_2d<default_graphics_traits_2d> default_graphics_2d;
	}

More documentation will follow as time permits, but priority is being given to the draft proposal, to the extent possible, as it provides a better vehicle for exploring the design, its decisions, and the rationales behind them, and for promoting critical thinking about these issues. It may also serve as the nucleus of an official proposal should this work reach that stage.

Note that determination of which of the above types will be movable, copyable, etc., has not been made. It's almost certain that the font_object, texture_2d, and brush types will all meet the requirements to be stored in a Standard Library container. It's also almost certain that all types will have move semantics. But nothing is fixed in stone, even though some aspects of the design are expected to be very easy to reach a consensus on.

## State of the Library

Work is on-going on a draft proposal and an implementation of it. Right now there is only one implementation the "initial implementation", written for Windows desktop using Win32, Direct2D, and DirectWrite. The initial implementation is complete enough that you can do things with it. Despite this there are things which are *missing*, *incomplete*, and a few that are even *improperly implemented*. Despite this, the initial implementation has reached a point where there is enough to show the fundamental structure envisioned for the library. It is template based with a traits class provided to enable multiple implementations to co-exist on the same platform. Resource creation and loading is separated from rendering, with the former accomplished via the graphics_device_2d class template and the latter via the graphics_context_2d class template. Various functionality is currently missing by intent in the hopes that discussion and suggestions might produce a better design than an arbitrary initial implementation would. Even the design decisions that were made in the initial implementation are open for discussion and suggestions and changes have already happened as a result of feedback (the scaling back of factory methods and removal of shared_ptrs from the library APIs (excluding internal implementation details)). 

## The Initial Implementation (a/k/a the Win32 Direct2D/DirectWrite Implementation)

The initial implementation is mostly undocumented. This is not fair, I know. The desire to release a rudimentary implementation to make discussion of it possible as soon as possible precluded early documentation. This does offer the benefit of seeing whether the specification seems intuitive to you.

The initial implementation is a provided in a Visual Studio 2013 solution. There are two projects included in the initial implementation. One is DirectXTK ( https://directxtk.codeplex.com/ ) which is used for texture loading. The other is called Graphics2D_TS_Win32. Please ignore the name and focus on the contents. If you want to start with looking at usage first, examine main.cpp. If you prefer to look at the specification, examine graphics_2d.h and graphics_traits_2d.h. All other files are implementation details. Even the member function implementations in the specification files are implementation details; if you aren't sure what that means, have a look at the ISO C++ Standard's specification of, e.g., vector, then look at how it is implemented in Visual C++, GCC's libstdc++, or Clang's libc++.

There are a couple of "abominations" in the initial implementation. It was unclear whether it was appropriate to use the std namespace and if so if it should go straight into the base namespace (i.e. ::std) or into a nested namespace (e.g. ::std::graphics). So it exists in a namespace called "std_graphics_2d_ts". On the bright side it will be pretty easy to do a find and replace when the correct namespace to use is learned. This is ultimately a matter for the ISO committee or one of its working groups, incidentally, so deference will be given to whatever they decide.

Also, the implementation is not a well-formed C++ program. The Microsoft CRT uses internal logic to determine the appropriate entry point for a program. The author of the initial implementation is unaware of any ways he can change the entry point and so the program contains a declaration for the main function. This is absolutely forbidden by the Standard. This declaration is in window.cpp (line 18). Since the Microsoft CRT is crafted to look for an entry point named WinMain for a Win32 program, this function exists in window.cpp. It simply records the HINSTANCE and CmdShow values in unguarded statics (another abomination, incidentally, since any static variables should be guarded) and then calls main. That way to C++ programmers who aren't looking at the implementation details, it just looks like a standard C++ program that begins at main (even though it doesn't). Thankfully VC++ doesn't explode when you do this!

Earlier there was mention of "missing, incomplete, and ... improperly implemented" features.

### The improperly implemented bits.

+	The mouse wheel functionality right now is fed Windows-specific values by this implementation. This could be specified in terms of degrees of rotation (such as Qt does). Or with the prevalence these days of track pads that can simulate mouse wheel scrolling, a fixed value might make more sense. In which case this would better be classified as "incomplete" since the value Windows uses is not necessarily the optimal value. Thoughts?

+	Keyboard input works but is only a place holder. There are at least two models to choose between. One model would have the library user receive all keyboard input events along with some sort of event type that specifies (at a minimum) whether it's a press or release. There should also be notation as to whether the event is a system-generated press (i.e. something from auto-repeat based on holding the key down indefinitely) if this can be done in a cross-platform way. The second model would be one where the user somehow subscribes with a list of keys/characters he or she cares about and is then fed back events based only on those. The initial implementation leans towards the first model since it only involves registering a general keyboard input handler and then leaves it up to the user how to interpret that data. The initial implementation fairly approximates the press portion of a Windows implementation of that model (though it does not discriminate between repeats and new presses). Since it doesn't have release semantics yet, it doesn't support things like using Shift or Control (and it doesn't include any support for Alt since that is a system key on Windows operating systems).

	There are probably also keys missing from the keyboard_keys enum which should be there. The implementation works fine with an IME (note: only tested with Japanese kana input using romaji to hiragana in Windows 8.1). Nonetheless, considerations for IMEs on all platforms should be made and adjustments to the library to accommodate IMEs on all platforms should be made.

+	DirectWrite happily takes in a font family name; FreeType seems to require a font file name. Since the initial implementation uses DirectWrite it accepts a font family name. The author has no brilliant insights about how to resolve this impasse between family name versus font file name. Also, the author has not had a chance to make any investigation into native font rendering on OS X and iOS.

+	Touch input is currently implemented as single-touch only and maps to a left mouse button event. This was a measure of expediency and not an intentional design pattern. We could either separate the two (and possibly split off pen/stylus into a third type) or go with a more holistic approach such as the "Pointer" concept used in the Windows Runtime (see generally: [Quickstart: Pointers (Windows Store apps using C#/VB/C++ and XAML](http://msdn.microsoft.com/en-us/library/windows/apps/jj150606.aspx)).

+	The preferredWindowTitle parameter for one of the window class constructors presently only supports a wstring argument when it should be written to support all acceptable string types.
It's strongly suggested (and the initial implementation makes the assumption) that the std::string and char * string types should be interpreted as UTF-8 only. Apologies to the code page die-hards out there. If anyone knows of any compelling arguments for including library support for code pages or other non-Unicode encodings (e.g. EBCDIC), please raise them.

### The incomplete bits.

+	The drawing APIs don't support any transformations (scaling, rotation, skewing, etc.). There are at least two ways to go on this: either create overloads which take such parameters (have a look at DirectXTK's SpriteBatch class for an example) or have the graphics context include some sort of internal state machine and add in API calls to it that set its parameters. The state machine model would probably be done via manipulation of an internal matrix, in which case we would need to decide on whether to use SRT or TRS order (Scaling, Rotation, Translation) and how to do skewing (if it's something that's desirable and cross-platform implementable, of course)).

+	The only (current) brush type is a solid color brush. There is some commented out code that hints, at a minimum, at support for a linear gradient brush and an image brush (aka bitmap aka texture). The image brush concept has a pitfall for users in that low powered graphics cards (in D3D terms, Feature Level 9\.x cards) will choke and die if you try to use a wrap mode (i.e. repeating the image over and over again) unless the image has power of 2 dimensions. That's no reason to exclude it an image brush with support for wrap mode, but it is something implementers need to know and which should get a defined behavior (such as a uniform exception with a uniform message across all platforms) to make it as clear as possible for library users what went wrong when their program attempts to violate this constraint. A compile time check would be preferred if it is practical, though with the potential that images could be streamed in at runtime rather than included with the project when built, this is likely impossible.

+	The keyboard_keys enum is probably missing some values. We probably can't get around having an enum for keys so this is considered incomplete. The Standard seems to require (or strongly prefer at the very least) unscoped enums and allows implementations to define the enum values as const values instead of placing them in an unscoped enum. Unless that preference has changed with the introduction of scoped enums, the scoped enums in the draft specification (and initial implementation) will need to be redone.

This list is not guaranteed to be exhaustive. Also, the line between incomplete and missing can be blurry so continue reading for other things which are classified as missing but which could also have been written up in a way that made them fit into incomplete instead.

### The missing bits.

+	Polygons. Want them? Yes. Have them (yet)? No.

+	Bezier curves.

+	Non-display rendering. It would be an alternate trait, but it is desirable that someone could use this API to do something like render to a file (e.g. tp a PNG, but also to other formats like PDF, OXPS, PostScript, etc., if there are willing implementers). The current design is oriented entirely to the concept of a window but that's because rendering is missing right now. It is intended to be a part of this library. Done right it might even be possible to "render" to a printer if someone wrote a suitable rendering engine. One key with non-display rendering is that it would also likely be non-interactive (i.e it would not have a window class instance associated with it). Users can't meaningfully interact with something they can't see. Optimally display and non-display will be able to co-exist in the same program and it'd be possible to take the commands to draw a display scene and send them to a non-display renderer (yes, the commands, not the rendered result). It would be nice if in the end something at least as advanced as WordPad could be created by a suitably motivated individual armed with the correct rendering engines.

## README Modification Log

Please update this if you make changes to it, including the date in YYYY-MM-DD format, the approximate time in UTC time (yes, please adjust the date if it is a different day in UTC time than it is where you live) with 24 hr notation (i.e. 21:42, not 09:42 PM), your name, and a summary of your changes.

2013-11-14 13:02	Michael B. McLaughlin
Updated to note that the D2DDWrite solution is now a Visual Studio 2013 solution.

2013-09-20 06:53 	Michael B. McLaughlin
Initial draft of document. Consists of some new information combined with a cleaned up, modified version of the email I sent out when I posted the original zip file prior to the availability of this repository.
