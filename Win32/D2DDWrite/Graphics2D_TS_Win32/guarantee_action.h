#pragma once
#include <functional>

// RAII wrapper that guarantees that the specified function will run when the object is destroyed.
class guarantee_action {
public:
	// Construct from a function by copy.
	guarantee_action(const std::function<void()>& fn)
		: m_function(fn) { }

	// Construct from a function by move.
	guarantee_action(std::function<void()>&& fn)
		: m_function(std::move(fn)) { }

	// Move constructor.
	guarantee_action(guarantee_action&& other)
		: m_function(nullptr) {
		m_function.swap(other.m_function);
	}

	// Move assignment operator.
	guarantee_action& operator=(guarantee_action&& other) {
		if (this != &other) {
			m_function = other.m_function;
			other.m_function = nullptr;
		}
		return (*this);
	}

	// Function copy assignment operator.
	guarantee_action& operator=(const std::function<void()>& fn) {
		m_function = fn;
	}

	// Function move assignment operator.
	guarantee_action& operator=(std::function<void()>&& fn) {
		m_function = std::move(fn);
	}

	// Destructor.
	~guarantee_action() {
		// If the function isn't null, run it. Casting it to bool invokes its explicit bool operator.
		if (static_cast<bool>(m_function) == true) {
			m_function();
		}
	}
private:
	// Disable copy construction.
	guarantee_action(const guarantee_action&); // = delete;

	// Disable copy assignment.
	guarantee_action& operator=(const guarantee_action&); // = delete;

	// The function to run.
	std::function<void()>							m_function;
};
