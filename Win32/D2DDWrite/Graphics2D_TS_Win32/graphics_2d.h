#pragma once
#ifndef _GRAPHICS_2D_
#define _GRAPHICS_2D_

#include <string>
#include <stdexcept>
#include <cstdint>
#include <memory>
#include <functional>
#include <unordered_map>
#include <mutex>
#include <algorithm>

#include "xgraphics_2d.h"
#include "graphics_traits_2d.h"

namespace std_graphics_2d_ts {
	class pixel_format;
	class no_graphics_2d_support;
	class window;

	template <class Traits>
	class graphics_traits_2d;

	template <class GraphicsTraits>
	class graphics_device_2d;

	template <class GraphicsTraits>
	class graphics_context_2d;

	template <class GraphicsTraits>
	class font_object;

	template <class GraphicsTraits>
	class texture_2d;

	template <class GraphicsTraits, class BrushType>
	class brush;

	template <class GraphicsTraits>
	class path_geometry;

	struct vector_2 {
		float x;
		float y;

		vector_2()
			: x()
			, y() {
		}
		vector_2(float x, float y)
			: x(x)
			, y(y) {
		}
	};

	struct rect {
		float x;
		float y;
		float width;
		float height;

		rect()
			: x()
			, y()
			, width()
			, height() {
		}
		rect(float x, float y, float width, float height)
			: x(x)
			, y(y)
			, width(width)
			, height(height) {
		}
	};

	struct rgba_unorm_color {
		float r;
		float g;
		float b;
		float a;

		rgba_unorm_color()
			: r()
			, g()
			, b()
			, a() {
		}

		rgba_unorm_color(std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t a)
			: r(r / static_cast<float>(UINT8_MAX))
			, g(g / static_cast<float>(UINT8_MAX))
			, b(b / static_cast<float>(UINT8_MAX))
			, a(a / static_cast<float>(UINT8_MAX)) {
		}

		rgba_unorm_color(const float color[4])
			: r(color[0])
			, g(color[1])
			, b(color[2])
			, a(color[3]) {
		}
	};

	typedef _D2d_dwrite_graphics default_graphics_traits_2d;
	typedef graphics_traits_2d<default_graphics_traits_2d> default_graphics_2d;

	class pixel_format {
	public:
		enum pixel_format_type : ::std::int32_t {
			none,
			native,
			r8_g8_b8_a8,
			b8_g8_r8_a8,
		};

		typedef DXGI_FORMAT native_format_type;

	private:
		pixel_format_type _Fmt;
		native_format_type _Native_fmt;

	public:
		explicit pixel_format(pixel_format_type fmt);

		explicit pixel_format(native_format_type fmt);

		native_format_type native_format() const;

		pixel_format_type format() const;
	};

	class no_graphics_2d_support : public ::std::runtime_error {
	public:
		explicit no_graphics_2d_support(const ::std::string& message)
			: ::std::runtime_error(message) {
		}

		explicit no_graphics_2d_support(const char* message)
			: ::std::runtime_error(message) {
		}
	};

	typedef ::std::uint_least32_t _Mouse_button_int_type;

	enum class mouse_button : _Mouse_button_int_type {
		unknown		= 0,
		none		= 1 << 0,
		left		= 1 << 1,
		right		= 1 << 2,
		middle		= 1 << 3,
		x1			= 1 << 4,
		x2			= 1 << 5,
	};

	inline mouse_button operator&(mouse_button l, mouse_button r) {
		return static_cast<mouse_button>(static_cast<_Mouse_button_int_type>(l) & static_cast<_Mouse_button_int_type>(r));
	}
	inline mouse_button operator|(mouse_button l, mouse_button r) {
		return static_cast<mouse_button>(static_cast<_Mouse_button_int_type>(l) | static_cast<_Mouse_button_int_type>(r));
	}
	inline mouse_button operator&=(mouse_button l, mouse_button r) {
		l = static_cast<mouse_button>(static_cast<_Mouse_button_int_type>(l) & static_cast<_Mouse_button_int_type>(r));
		return l;
	}
	inline mouse_button operator|=(mouse_button l, mouse_button r) {
		l = static_cast<mouse_button>(static_cast<_Mouse_button_int_type>(l) | static_cast<_Mouse_button_int_type>(r));
		return l;
	}

	enum class mouse_event_type : ::std::int_least16_t {
		unknown,
		button_down,
		button_up,
		double_click,
		move,
		wheel,
		horizontal_wheel,
	};

	enum class keyboard_key : ::std::int_least16_t {
		unknown = 0,
		character_value, // Used when we get a translated character value.
		left,
		right,
		up,
		down,
		home,
		end,
		insert,
		delete_key,
		page_up,
		page_down,
		f1,
		f2,
		f3,
		f4,
		f5,
		f6,
		f7,
		f8,
		f9,
		f10,
		f11,
		f12,
		f13,
		f14,
		f15,
		f16,
		f17,
		f18,
		f19,
		f20,
		f21,
		f22,
		f23,
		f24,
		enter,
		num_lock,
		caps_lock,
		shift,
		backspace,
		alt,
		alt_gr,
		control,
		platform_key,
		meta,
		escape,
		tab,
	};

	struct keyboard_event {
		keyboard_key key;
		wchar_t character;
	};

	class window {
	public:
		typedef _Wndw_t::native_handle_type native_handle_type;

	private:
		std::shared_ptr<_Wndw_t> _Wndw;

		::std::function<void()> _Draw_fn;

		::std::function<void(mouse_event_type event_type, mouse_button button, const vector_2& coordinates)> _Mouse_fn;
		::std::function<void(mouse_event_type event_type, mouse_button button, ::std::int32_t value)> _Mouse_wheel_fn;
		::std::function<void(keyboard_event keyEvent)> _Keyboard_event_fn;

	public:
		window(::std::int32_t preferredWidth, ::std::int32_t preferredHeight, const char* preferredWindowTitle);
		window(::std::int32_t preferredWidth, ::std::int32_t preferredHeight, const wchar_t* preferredWindowTitle);
		window(::std::int32_t preferredWidth, ::std::int32_t preferredHeight, const ::std::string& preferredWindowTitle);
		window(::std::int32_t preferredWidth, ::std::int32_t preferredHeight, const ::std::wstring& preferredWindowTitle);

		explicit window(native_handle_type handle);

		~window();

		native_handle_type native_handle() const;

		bool is_valid() const;

		void run_event_loop();

		void set_draw_function(const ::std::function<void()>& fn);
		void set_draw_function(::std::function<void()>&& fn);

		void set_mouse_function(const ::std::function<void(mouse_event_type eventType, mouse_button button, const vector_2& coordinates)>& fn);
		void set_mouse_function(::std::function<void(mouse_event_type eventType, mouse_button button, const vector_2& coordinates)>&& fn);

		void set_mouse_wheel_function(const ::std::function<void(mouse_event_type eventType, mouse_button button, ::std::int32_t value)>& fn);
		void set_mouse_wheel_function(::std::function<void(mouse_event_type eventType, mouse_button button, ::std::int32_t value)>&& fn);

		void set_keyboard_function(const ::std::function<void(keyboard_event keyEvent)>& fn);
		void set_keyboard_function(::std::function<void(keyboard_event keyEvent)>&& fn);

		void call_mouse_function(mouse_event_type eventType, mouse_button button, const vector_2& coordinates);
		void call_mouse_wheel_function(mouse_event_type eventType, mouse_button button, ::std::int32_t value);
		void call_keyboard_function(keyboard_event keyEvent);

		void draw();

		::std::int32_t get_width() const;
		::std::int32_t get_height() const;
	};

	template <class GraphicsTraits>
	class graphics_device_2d {
	public:
		typedef typename GraphicsTraits::traits_type traits_type;
		typedef typename GraphicsTraits::device_type device;
		typedef typename device::native_handle_type native_handle_type;

	private:
		::std::shared_ptr<device> _Dvce;
		GraphicsTraits _Graphics_traits;

	public:
		graphics_device_2d(const window& window, const pixel_format& preferredFormat)
			: _Dvce(new device(window, preferredFormat))
			, _Graphics_traits(traits_type()) {
		}

		graphics_context_2d<GraphicsTraits> create_graphics_context_2d() {
			return graphics_context_2d<GraphicsTraits>(*_Dvce.get());
		}

		native_handle_type native_handle() const {
			return _Dvce->native_handle();
		}

		bool has_a_display_surface() const {
			return _Graphics_traits.has_a_display_surface();
		}

		template <class StringType>
		texture_2d<GraphicsTraits> load_texture_2d(const StringType& filename) {
			return _Dvce->load_texture_2d(filename);
		}

		texture_2d<GraphicsTraits> load_texture_2d_from_memory(const ::std::unique_ptr<::std::uint8_t[]>& data, const pixel_format& format) {
			return _Dvce->load_texture_2d_from_memory(data, format);
		}

		template <class StringType>
		font_object<GraphicsTraits> create_font_object(const StringType& fontname, float pointSize) {
			return _Dvce->create_font_object(fontname, pointSize);
		}

		typename GraphicsTraits::solid_color_brush create_solid_color_brush(const std_graphics_2d_ts::rgba_unorm_color& color) {
			return _Dvce->create_solid_color_brush(color);
		}

		path_geometry<GraphicsTraits> create_path_geometry() {
			return _Dvce->create_path_geometry();
		}
	};

	template <class GraphicsTraits>
	class graphics_context_2d {
	public:
		typedef typename GraphicsTraits::device_type device_type;
		typedef typename GraphicsTraits::context_type context;
		typedef typename GraphicsTraits::font_object_type font_type;
		typedef typename context::native_handle_type native_handle_type;

	private:
		::std::shared_ptr<context> _Ctxt;

	public:
		explicit graphics_context_2d(const device_type& dvce)
			: _Ctxt(new context(dvce)) { }

		native_handle_type native_handle() const {
			return _Ctxt->native_handle();
		}

		void begin_drawing() {
			return _Ctxt->begin_drawing();
		}

		void end_drawing() {
			return _Ctxt->end_drawing();
		}

		void clear_drawing_surface(const rgba_unorm_color& color) {
			return _Ctxt->clear_drawing_surface(color);
		}

		template <class Texture2D>
		void draw_texture_2d(const Texture2D& texture, const vector_2& dest) {
			return _Ctxt->draw_texture_2d(texture, dest);
		}

		template <class StringType, class BrushType, class FontObject>
		void draw_string(const FontObject& font, const StringType& str, const rect& dest, const BrushType brush) {
			return _Ctxt->draw_string(font, str, dest, brush);
		}

		template <class BrushType>
		void draw_line(const vector_2& start, const vector_2& end, const BrushType brush, float strokeWidth) {
			return _Ctxt->draw_line(start, end, brush, strokeWidth);
		}

		template <class BrushType>
		void draw_rect(const rect& rect, const BrushType brush, float strokeWidth) {
			return _Ctxt->draw_rect(rect, brush, strokeWidth);
		}

		template <class BrushType>
		void fill_rect(const rect& rect, const BrushType brush) {
			return _Ctxt->fill_rect(rect, brush);
		}

		template <class BrushType>
		void draw_ellipse(const vector_2& center, float xRadius, float yRadius, const BrushType brush, float strokeWidth) {
			return _Ctxt->draw_ellipse(center, xRadius, yRadius, brush, strokeWidth);
		}

		template <class BrushType>
		void fill_ellipse(const vector_2& center, float xRadius, float yRadius, const BrushType& brush) {
			return _Ctxt->fill_ellipse(center, xRadius, yRadius, brush);
		}

		template <class BrushType>
		void draw_path_geometry(const path_geometry<GraphicsTraits>& pathGeometry, const BrushType& brush, float strokeWidth) {
			return _Ctxt->draw_path_geometry(pathGeometry, brush, strokeWidth);
		}

		template <class BrushType>
		void fill_path_geometry(const path_geometry<GraphicsTraits>& pathGeometry, const BrushType& brush) {
			return _Ctxt->fill_path_geometry(pathGeometry, brush);
		}

		template <class BrushType>
		void draw_bezier(const vector_2& fromPoint, const vector_2& toPoint, const vector_2& controlPoint1, const vector_2& controlPoint2, const BrushType& brush, float strokeWidth) {
			return _Ctxt->draw_bezier(fromPoint, toPoint, controlPoint1, controlPoint2, brush, strokeWidth);
		}
	};

	template <class GraphicsTraits>
	class texture_2d {
	public:
		typedef typename GraphicsTraits::texture_2d_type texture;
		typedef typename texture::native_handle_type native_handle_type;

	private:
		::std::shared_ptr<texture> _Texture;

	public:
		texture_2d(const ::std::shared_ptr<texture>& texture)
			: _Texture(texture) {
		}

		native_handle_type native_handle() const {
			return _Texture->native_handle();
		}
	};

	template <class GraphicsTraits>
	class font_object {
	public:
		typedef typename GraphicsTraits::font_object_type font;
		typedef typename font::native_handle_type native_handle_type;

	private:
		::std::shared_ptr<font> _Font;

	public:
		font_object(font&& fnt)
			: _Font(new font(fnt)) { }

		native_handle_type native_handle() const {
			return _Font->native_handle();
		}
	};

	template <class GraphicsTraits, class BrushType>
	class brush {
	public:
		typedef typename BrushType::native_handle_type native_handle_type;

	private:
		::std::shared_ptr<BrushType> _Brush;

	public:
		brush()
			: _Brush() {
		}
		brush(const std::shared_ptr<BrushType>& brush)
			: _Brush(brush) {
		}

		native_handle_type native_handle() const {
			return _Brush->native_handle();
		}
	};

	template <class GraphicsTraits>
	class path_geometry {
	public:
		typedef typename GraphicsTraits::path_geometry_type path_geometry_type;
		typedef typename path_geometry_type::native_handle_type native_handle_type;

	private:
		::std::shared_ptr<path_geometry_type> _PathGeometry;
	public:
		path_geometry(path_geometry_type&& path)
			: _PathGeometry(new path_geometry_type(path)) { }

		native_handle_type native_handle() const {
			return _PathGeometry->native_handle();
		}

		void begin_define(const vector_2& beginPoint) {
			return _PathGeometry->begin_define(beginPoint);
		}

		void end_define() {
			return _PathGeometry->end_define();
		}

		void add_line(const vector_2& toPoint) {
			return _PathGeometry->add_line(toPoint);
		}

		void add_bezier(const vector_2& toPoint, const vector_2& controlPoint1, const vector_2& controlPoint2) {
			return _PathGeometry->add_bezier(toPoint, controlPoint1, controlPoint2);
		}
	};
}

#endif
