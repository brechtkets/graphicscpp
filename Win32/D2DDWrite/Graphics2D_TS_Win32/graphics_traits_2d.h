#pragma once
#ifndef _GRAPHICS_2D_TRAITS_H
#define _GRAPHICS_2D_TRAITS_H

namespace std_graphics_2d_ts {
	// Forward declarations.
	class pixel_format;
	class no_graphics_2d_support;
	class window;

	template <class GraphicsTraits>
	class graphics_device_2d;

	template <class GraphicsTraits>
	class graphics_context_2d;

	struct rgba_unorm_color;
	struct vector_2;
	struct rect;
	template <class GraphicsTraits>
	class texture_2d;

	template <class GraphicsTraits>
	class font_object;

	template <class GraphicsTraits, class BrushType>
	class brush;

	template <class GraphicsTratis>
	class path_geometry;

	template <class Traits>
	class graphics_traits_2d {
		Traits _Graphics_traits;

	public:
		typedef typename Traits traits_type;
		typedef typename Traits::device_type device_type;
		typedef ::std_graphics_2d_ts::graphics_device_2d<::std_graphics_2d_ts::graphics_traits_2d<Traits>> device;

		typedef typename Traits::context_type context_type;
		typedef ::std_graphics_2d_ts::graphics_context_2d<::std_graphics_2d_ts::graphics_traits_2d<Traits>> context;

		typedef typename Traits::texture_2d_type texture_2d_type;
		typedef ::std_graphics_2d_ts::texture_2d<::std_graphics_2d_ts::graphics_traits_2d<Traits>> texture_2d;

		typedef typename Traits::font_object_type font_object_type;
		typedef ::std_graphics_2d_ts::font_object<Traits> font_object;

		typedef typename Traits::solid_color_brush_type solid_color_brush_type;
		typedef ::std_graphics_2d_ts::brush<::std_graphics_2d_ts::graphics_traits_2d<Traits>, solid_color_brush_type> solid_color_brush;

		//typedef typename Traits::linear_gradient_brush_type linear_gradient_brush_type;
		//typedef ::std_graphics_2d_ts::brush<::std_graphics_2d_ts::graphics_traits_2d<Traits>, linear_gradient_brush_type> linear_gradient_brush;
		
		//typedef typename Traits::image_brush_type image_brush_type;
		//typedef ::std_graphics_2d_ts::brush<::std_graphics_2d_ts::graphics_traits_2d<Traits>, image_brush_type> image_brush;

		typedef typename Traits::path_geometry_type path_geometry_type;
		typedef ::std_graphics_2d_ts::path_geometry<::std_graphics_2d_ts::graphics_traits_2d<Traits>> path_geometry;

		explicit graphics_traits_2d(const Traits& _Traits)
			: _Graphics_traits(_Traits) {
		}

		bool has_a_display_surface() const {
			return _Graphics_traits.has_a_display_surface();
		}
	};
}

#endif
