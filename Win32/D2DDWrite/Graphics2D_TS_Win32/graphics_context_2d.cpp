#include "stdafx.h"
#include "graphics_2d.h"

using namespace std_graphics_2d_ts;

_D2d_dwrite_grphcs_ctxt_impl::_D2d_dwrite_grphcs_ctxt_impl(const _D2d_dwrite_graphics::device_type& device)
	: _Device_data(device.get_device_data()) {
}

_D2d_dwrite_grphcs_ctxt_impl::native_handle_type _D2d_dwrite_grphcs_ctxt_impl::native_handle() const {
	return _Device_data->_Direct2DContext.Get();
}

void _D2d_dwrite_grphcs_ctxt_impl::begin_drawing() {
	auto context = _Device_data->_D3DContext.Get();
	auto commonStates = _Device_data->_CommonStates.get();
	context->OMSetRenderTargets(1, _Device_data->_D3DRTV.GetAddressOf(), nullptr);
	context->RSSetState(commonStates->CullCounterClockwise());
	context->OMSetBlendState(commonStates->Opaque(), nullptr, UINT32_MAX);
	context->OMSetDepthStencilState(commonStates->DepthNone(), 0);
	_Device_data->_Direct2DContext->BeginDraw();
}

void _D2d_dwrite_grphcs_ctxt_impl::end_drawing() {
	HRESULT hr;
	hr = _Device_data->_Direct2DContext->EndDraw();
	if (FAILED(hr)) {
		std::stringstream str;
		str << "Error calling ID2D1DeviceContext::EndDraw. HR = 0x" << std::hex << std::uppercase << hr;
		throw ::std::runtime_error(str.str());
	}

	hr = _Device_data->_SwpChn->Present(1, 0);
	if (FAILED(hr)) {
		std::stringstream str;
		str << "Error calling IDXGISwapChain::Present. HR = 0x" << std::hex << std::uppercase << hr;
		throw ::std::runtime_error(str.str());
	}
}

inline float cap_color_channel_to_norm(float value) {
	return ::std::max(0.0f, ::std::min(1.0f, value));
}

void _D2d_dwrite_grphcs_ctxt_impl::clear_drawing_surface(const rgba_unorm_color& color) {
	D2D1_COLOR_F clearColor;
	clearColor.r = cap_color_channel_to_norm(color.r);
	clearColor.g = cap_color_channel_to_norm(color.g);
	clearColor.b = cap_color_channel_to_norm(color.b);
	clearColor.a = cap_color_channel_to_norm(color.a);
	_Device_data->_Direct2DContext->Clear(clearColor);
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_texture_2d(const texture_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>>& texture, const vector_2& dest) {
	auto bitmap = static_cast<ID2D1Bitmap *>(texture.native_handle());
	auto size = bitmap->GetSize();
	float dpiX, dpiY;
	_Device_data->_Direct2DFactory->GetDesktopDpi(&dpiX, &dpiY);
	dpiX /= 96.0f;
	dpiY /= 96.0f;
	_Device_data->_Direct2DContext->DrawBitmap(bitmap, D2D1::RectF(dest.x * dpiX, dest.y * dpiY, (dest.x + size.width) * dpiX, (dest.y + size.height) * dpiY));
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_string(const font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>>& font, const char* str, const rect& dest, const _D2d_dwrite_graphics::solid_color_brush& brush) {
	draw_string(font, ::std::string(str), dest, brush);
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_string(const font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>>& font, const wchar_t* str, const rect& dest, const _D2d_dwrite_graphics::solid_color_brush& brush) {
	draw_string(font, ::std::wstring(str), dest, brush);
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_string(const font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>>& font, const ::std::string& str, const rect& dest, const _D2d_dwrite_graphics::solid_color_brush& brush) {
	auto wstr = _UTF8_to_wstring(str);
	draw_string(font, wstr, dest, brush);
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_string(const font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>>& font, const ::std::wstring& str, const rect& dest, const _D2d_dwrite_graphics::solid_color_brush& brush) {
	auto layoutRect = D2D1::RectF(dest.x, dest.y, dest.x + dest.width, dest.y + dest.height);
	_Device_data->_Direct2DContext->DrawText(str.c_str(), static_cast<UINT32>(str.length()), static_cast<IDWriteTextFormat *>(font.native_handle()), layoutRect, brush.native_handle());
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_line(const vector_2& start, const vector_2& end, const _D2d_dwrite_graphics::solid_color_brush& brush, float width) {
	_Device_data->_Direct2DContext->DrawLine(D2D1::Point2F(start.x, start.y), D2D1::Point2F(end.x, end.y), brush.native_handle(), width);
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_rect(const rect& rect, const _D2d_dwrite_graphics::solid_color_brush& brush, float width) {
	_Device_data->_Direct2DContext->DrawRectangle(D2D1::RectF(rect.x, rect.y, rect.x + rect.width, rect.y + rect.height), brush.native_handle(), width);
}

void _D2d_dwrite_grphcs_ctxt_impl::fill_rect(const rect& rect, const _D2d_dwrite_graphics::solid_color_brush& brush) {
	_Device_data->_Direct2DContext->FillRectangle(D2D1::RectF(rect.x, rect.y, rect.x + rect.width, rect.y + rect.height), brush.native_handle());
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_ellipse(const ::std_graphics_2d_ts::vector_2& center, float xRadius, float yRadius, const _D2d_dwrite_graphics::solid_color_brush& brush, float strokeWidth) {
	_Device_data->_Direct2DContext->DrawEllipse(D2D1::Ellipse(D2D1::Point2F(center.x, center.y), xRadius, yRadius), brush.native_handle(), strokeWidth);
}

void _D2d_dwrite_grphcs_ctxt_impl::fill_ellipse(const ::std_graphics_2d_ts::vector_2& center, float xRadius, float yRadius, const _D2d_dwrite_graphics::solid_color_brush& brush) {
	_Device_data->_Direct2DContext->FillEllipse(D2D1::Ellipse(D2D1::Point2F(center.x, center.y), xRadius, yRadius), brush.native_handle());
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_path_geometry(const _D2d_dwrite_graphics::path_geometry& pathGeometry, const _D2d_dwrite_graphics::solid_color_brush& brush, float strokeWidth) {
	_Device_data->_Direct2DContext->DrawGeometry(pathGeometry.native_handle(), brush.native_handle(), strokeWidth);
}

void _D2d_dwrite_grphcs_ctxt_impl::fill_path_geometry(const _D2d_dwrite_graphics::path_geometry& pathGeometry, const _D2d_dwrite_graphics::solid_color_brush& brush) {
	_Device_data->_Direct2DContext->FillGeometry(pathGeometry.native_handle(), brush.native_handle());
}

void _D2d_dwrite_grphcs_ctxt_impl::draw_bezier(const ::std_graphics_2d_ts::vector_2& fromPoint, const ::std_graphics_2d_ts::vector_2& toPoint, const ::std_graphics_2d_ts::vector_2& controlPoint1, const ::std_graphics_2d_ts::vector_2& controlPoint2, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush, float strokeWidth) {
	Microsoft::WRL::ComPtr<ID2D1PathGeometry> pathGeometry;
	throw_if_failed_hresult<::std::runtime_error>(
		_Device_data->_Direct2DFactory->CreatePathGeometry(&pathGeometry),
		"Failed call to ID2D1Factory::CreatePathGeometry."
		);
	Microsoft::WRL::ComPtr<ID2D1GeometrySink> geometrySink;
	throw_if_failed_hresult<::std::runtime_error>(
		pathGeometry->Open(&geometrySink),
		"Failed call to ID2D1PathGeometry::Open."
		);
	geometrySink->BeginFigure(D2D1::Point2F(fromPoint.x, fromPoint.y), D2D1_FIGURE_BEGIN_HOLLOW);
	geometrySink->AddBezier(D2D1::BezierSegment(D2D1::Point2F(controlPoint1.x, controlPoint1.y), D2D1::Point2F(controlPoint2.x, controlPoint2.y), D2D1::Point2F(toPoint.x, toPoint.y)));
	geometrySink->EndFigure(D2D1_FIGURE_END_OPEN);

	throw_if_failed_hresult<::std::runtime_error>(
		geometrySink->Close(),
		"Failed call to ID2D1SimplifiedGeometrySink::Close."
		);

	_Device_data->_Direct2DContext->DrawGeometry(pathGeometry.Get(), brush.native_handle(), strokeWidth);
}
