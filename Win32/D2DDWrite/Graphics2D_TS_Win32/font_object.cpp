#include "stdafx.h"

#include "graphics_2d.h"

_D2d_dwrite_font_object_impl::native_handle_type _D2d_dwrite_font_object_impl::native_handle() const {
	return static_cast<_D2d_dwrite_font_object_impl::native_handle_type>(_TextFormat.Get());
}