#include "stdafx.h"
#include "graphics_2d.h"
#include <dxgi.h>

using namespace std_graphics_2d_ts;

pixel_format::pixel_format(pixel_format::pixel_format_type fmt) {
	_Fmt = fmt;
	_Native_fmt = DXGI_FORMAT_UNKNOWN;
}

pixel_format::pixel_format(pixel_format::native_format_type fmt) {
	_Fmt = pixel_format_type::native;
	_Native_fmt = fmt;
}

DXGI_FORMAT pixel_format::native_format() const {
	return _Native_fmt;
}

pixel_format::pixel_format_type pixel_format::format() const {
	return _Fmt;
}