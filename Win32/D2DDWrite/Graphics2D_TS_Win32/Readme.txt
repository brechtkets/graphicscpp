If you wish to make changes, please familiarize yourself with 17.5 [description] from the C++ standard (you can obtain the most recent working draft from http://www.isocpp.org/ ) and with the guidance on this webpage: http://open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3370.html . Keeping to the style requirements and guidelines is important (though minor errors will certainly creep in at times and be removed at times).

The core library API consists of:

namespace std_graphics_2d_ts {
	class pixel_format;
	class no_graphics_2d_support;
	class window;

	template <class Traits>
	class graphics_traits_2d;

	template <class GraphicsTraits>
	class graphics_device_2d;

	template <class GraphicsTraits>
	class graphics_context_2d;

	template <class GraphicsTraits>
	class font_object;

	template <class GraphicsTraits>
	class texture_2d;

	template <class GraphicsTraits, class BrushType>
	class brush;

	struct vector_2;

	struct rect;

	struct rgba_unorm_color;

	// unknown is a class type that can be used as a template argument to graphics_traits_2d.
	typedef unknown default_graphics_traits_2d; 

	typedef graphics_traits_2d<default_graphics_traits_2d> default_graphics_2d;
}

More documentation to follow as possible.

Note that determination of which types will be movable, copyable, etc., has not been made yet.
It's almost certain that the font_object, texture_2d, and brush types will all meet the
requirements to be stored in an STL container.

