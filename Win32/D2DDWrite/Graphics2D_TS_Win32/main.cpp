#include "stdafx.h"
#include "graphics_2d.h"
#include <DirectXColors.h>

using namespace std_graphics_2d_ts;
using namespace std;

int main(int /*argc*/, char** /*argv*/) {
	auto wndw = window(1280, 720, "Hello Graphics!");
	auto device = graphics_device_2d<default_graphics_2d>(wndw, pixel_format(pixel_format::b8_g8_r8_a8));
	auto context = device.create_graphics_context_2d();
	auto texture = device.load_texture_2d("cat.png");
	auto fontObj = device.create_font_object("Segoe UI", 72.0f);
	auto pathGeometry = device.create_path_geometry();
	pathGeometry.begin_define(vector_2(100.0f, 500.0f));
	pathGeometry.add_line(vector_2(300.0f, 500.0f));
	pathGeometry.add_bezier(vector_2(300.0f, 599.0f), vector_2(340.0f, 533.0f), vector_2(260.0f, 566.0f));
	pathGeometry.add_line(vector_2(100.0f, 599.0f));
	pathGeometry.end_define();

	// Note, the example uses DirectX::Colors::xxxxxxxx which is an XMVECTORF32 which has a const float* operator
	// that gives the color in RGBA unorm format (i.e. it's an array of 4 floats). There is no dependency on
	// DirectX, just the ability to initialize with an argument that is a const float color[4].
	auto blackBrush = device.create_solid_color_brush(rgba_unorm_color(DirectX::Colors::Black));
	auto redBrush = device.create_solid_color_brush(rgba_unorm_color(DirectX::Colors::Red));
	auto yellowBrush = device.create_solid_color_brush(rgba_unorm_color(DirectX::Colors::Yellow));
	auto blueBrush = device.create_solid_color_brush(rgba_unorm_color(0, 0, 0xFF, 0xFF));
	vector_2 texturePosition(20.0f, 20.0f);
	auto arigato = std::wstring(L"\x3042\x308A\x304C\x3068\x3046");

	wndw.set_draw_function([&]() {
		context.begin_drawing();
		context.clear_drawing_surface(rgba_unorm_color(DirectX::Colors::CornflowerBlue));
		context.fill_ellipse(vector_2(220.0f, 570.0f), 200.0f, 150.0f, blueBrush);
		context.draw_ellipse(vector_2(800.0f, 120.0f), 100.0f, 100.0f, redBrush, 5.0f);
		context.draw_texture_2d(texture, texturePosition);
		context.draw_string(fontObj, "Hello Graphics!", rect(60.0f, 310.0f, 600.0f, 100.0f), redBrush);
		context.fill_rect(rect(600.0f, 400.0f, 200.0f, 200.0f), redBrush);
		context.draw_rect(rect(600.0f, 400.0f, 200.0f, 200.0f), blackBrush, 2.0f);
		context.draw_line(vector_2(0.0f, 360.5f), vector_2(1280.0f, 360.5f), yellowBrush, 1.0f);
		context.draw_bezier(vector_2(0.0f, 360.5f), vector_2(1280.0f, 360.5f), vector_2(426.6667f, 490.5f), vector_2(853.3333f, 490.5f), blueBrush, 3.0f);
		context.draw_string(fontObj, arigato, rect(500.0f, 30.0f, 600.0f, 600.0f), blackBrush);
		// Note: The hiragana for sensei in UTF-8.
		context.draw_string(fontObj, "\xe3\x81\x9b\xe3\x82\x93\xe3\x81\x9b\xe3\x81\x83!", rect(900.0f, 620.0f, 600.0f, 600.0f), blueBrush);
		context.fill_path_geometry(pathGeometry, redBrush);
		//context.draw_path_geometry(pathGeometry, blackBrush, 4.0f);
		context.end_drawing();
	});

	wndw.set_mouse_function([&](mouse_event_type eventType, mouse_button button, const vector_2& coordinates) {
		static bool isValid = false;

		switch (eventType) {
		case std_graphics_2d_ts::mouse_event_type::unknown:
			break;
		case std_graphics_2d_ts::mouse_event_type::button_down:
			if (button == mouse_button::left) {
				isValid = true;
				texturePosition.x = coordinates.x;
				texturePosition.y = coordinates.y;
			}
			break;
		case std_graphics_2d_ts::mouse_event_type::button_up:
			if (button == mouse_button::left) {
				isValid = false;
			}
			break;
		case std_graphics_2d_ts::mouse_event_type::move:
			if (isValid) {
				texturePosition.x = coordinates.x;
				texturePosition.y = coordinates.y;
			}
			break;
		default:
			break;
		}
	});

		wndw.set_keyboard_function([&](keyboard_event keyEvent) {
		const float delta = 10.0f;
		switch (keyEvent.key)
		{
		case keyboard_key::backspace:
			if (arigato.length() > 0) {
				arigato.erase(arigato.cend() - 1);
			}
			break;
		case keyboard_key::character_value:
			arigato.push_back(keyEvent.character);
			break;
		case keyboard_key::left:
			texturePosition.x -= delta;
			break;
		case keyboard_key::right:
			texturePosition.x += delta;
			break;
		case keyboard_key::up:
			texturePosition.y -= delta;
			break;
		case keyboard_key::down:
			texturePosition.y += delta;
			break;
		case keyboard_key::enter:
			texturePosition.x = 20.0f;
			texturePosition.y = 20.0f;
			break;
		default:
			break;
		}
	});

	wndw.run_event_loop();
}
