#include "stdafx.h"
#include "graphics_2d.h"
#include <assert.h>
#include <sstream>
#include <windowsx.h>
#include "guarantee_action.h"

using namespace std_graphics_2d_ts;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HINSTANCE _Wndw_impl_t::_Hinst;

int _Wndw_impl_t::_CmdShow;

// Note: This violates the C++ standard. When implemented by VC++, the Microsoft CRT could be adjusted accordingly to
// remove the need for this.
int main(int argc, char** argv);

window::window(::std::int32_t preferredWidth, ::std::int32_t preferredHeight, const char* preferredWindowTitle)
	: _Wndw(new _Wndw_t())
	, _Draw_fn()
	, _Mouse_fn()
	, _Mouse_wheel_fn()
	, _Keyboard_event_fn() {
		_Wndw->_Width = preferredWidth;
		_Wndw->_Height = preferredHeight;
		assert(preferredWidth > 0 && preferredHeight > 0);

		auto windowTitle = _UTF8_to_wstring(std::string(preferredWindowTitle));
		WNDCLASSEX wcex = {};
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = &_Wndw_t::WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = sizeof(window*);
		wcex.hInstance = _Wndw->_Hinst;
		wcex.hIcon = nullptr;
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = nullptr;
		wcex.lpszClassName = windowTitle.c_str();
		wcex.hIconSm = nullptr;

		ATOM atom = RegisterClassEx(&wcex);

		if (atom == 0) {
			throw_get_last_error<no_graphics_2d_support>("Could not register window class.");
		}

		RECT clientRect = { 0, 0, static_cast<LONG>(preferredWidth), static_cast<LONG>(preferredHeight) };
		if (AdjustWindowRect(&clientRect, WS_OVERLAPPEDWINDOW, FALSE) == FALSE) {
			throw_get_last_error<::std::runtime_error>("GetClientRect call failed on window that was just created.");
		}
		preferredWidth = static_cast<::std::int32_t>(clientRect.right - clientRect.left);
		preferredHeight = static_cast<::std::int32_t>(clientRect.bottom - clientRect.top);
		_Wndw->_Hwnd = CreateWindowExW(
			0L,
			wcex.lpszClassName,
			windowTitle.c_str(),
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			0,
			clientRect.right - clientRect.left,
			clientRect.bottom - clientRect.top,
			NULL,
			NULL,
			_Wndw->_Hinst,
			static_cast<LPVOID>(this)
			);

		if (_Wndw->_Hwnd == nullptr) {
			throw_get_last_error<no_graphics_2d_support>("Could not create window.");
		}
		if (RegisterTouchWindow(_Wndw->_Hwnd, 0) == FALSE) {
			throw_get_last_error<::std::logic_error>("Failed call to RegisterTouchWindow.");
		}
		ShowWindow(_Wndw->_Hwnd, _Wndw->_CmdShow);
		UpdateWindow(_Wndw->_Hwnd);
}

window::window(::std::int32_t preferredWidth, ::std::int32_t preferredHeight, const wchar_t* preferredWindowTitle)
	: _Wndw(new _Wndw_t())
	, _Draw_fn()
	, _Mouse_fn()
	, _Mouse_wheel_fn()
	, _Keyboard_event_fn() {
		_Wndw->_Width = preferredWidth;
		_Wndw->_Height = preferredHeight;
		assert(preferredWidth > 0 && preferredHeight > 0);

		::std::wstring windowTitle(preferredWindowTitle);

		WNDCLASSEX wcex = {};
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = &_Wndw_t::WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = sizeof(window*);
		wcex.hInstance = _Wndw->_Hinst;
		wcex.hIcon = nullptr;
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = nullptr;
		wcex.lpszClassName = windowTitle.c_str();
		wcex.hIconSm = nullptr;

		ATOM atom = RegisterClassEx(&wcex);

		if (atom == 0) {
			throw_get_last_error<no_graphics_2d_support>("Could not register window class.");
		}

		RECT clientRect = { 0, 0, static_cast<LONG>(preferredWidth), static_cast<LONG>(preferredHeight) };
		if (AdjustWindowRect(&clientRect, WS_OVERLAPPEDWINDOW, FALSE) == FALSE) {
			throw_get_last_error<::std::runtime_error>("GetClientRect call failed on window that was just created.");
		}
		preferredWidth = static_cast<::std::int32_t>(clientRect.right - clientRect.left);
		preferredHeight = static_cast<::std::int32_t>(clientRect.bottom - clientRect.top);
		_Wndw->_Hwnd = CreateWindowExW(
			0L,
			wcex.lpszClassName,
			windowTitle.c_str(),
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			0,
			clientRect.right - clientRect.left,
			clientRect.bottom - clientRect.top,
			NULL,
			NULL,
			_Wndw->_Hinst,
			static_cast<LPVOID>(this)
			);

		if (_Wndw->_Hwnd == nullptr) {
			throw_get_last_error<no_graphics_2d_support>("Could not create window.");
		}
		if (RegisterTouchWindow(_Wndw->_Hwnd, 0) == FALSE) {
			throw_get_last_error<::std::logic_error>("Failed call to RegisterTouchWindow.");
		}
		ShowWindow(_Wndw->_Hwnd, _Wndw->_CmdShow);
		UpdateWindow(_Wndw->_Hwnd);
}

window::window(::std::int32_t preferredWidth, ::std::int32_t preferredHeight, const ::std::string& preferredWindowTitle)
	: _Wndw(new _Wndw_t())
	, _Draw_fn()
	, _Mouse_fn()
	, _Mouse_wheel_fn()
	, _Keyboard_event_fn() {
		_Wndw->_Width = preferredWidth;
		_Wndw->_Height = preferredHeight;
		assert(preferredWidth > 0 && preferredHeight > 0);

		auto windowTitle = _UTF8_to_wstring(preferredWindowTitle);

		WNDCLASSEX wcex = {};
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = &_Wndw_t::WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = sizeof(window*);
		wcex.hInstance = _Wndw->_Hinst;
		wcex.hIcon = nullptr;
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = nullptr;
		wcex.lpszClassName = windowTitle.c_str();
		wcex.hIconSm = nullptr;

		ATOM atom = RegisterClassEx(&wcex);

		if (atom == 0) {
			throw_get_last_error<no_graphics_2d_support>("Could not register window class.");
		}

		RECT clientRect = { 0, 0, static_cast<LONG>(preferredWidth), static_cast<LONG>(preferredHeight) };
		if (AdjustWindowRect(&clientRect, WS_OVERLAPPEDWINDOW, FALSE) == FALSE) {
			throw_get_last_error<::std::runtime_error>("GetClientRect call failed on window that was just created.");
		}
		preferredWidth = static_cast<::std::int32_t>(clientRect.right - clientRect.left);
		preferredHeight = static_cast<::std::int32_t>(clientRect.bottom - clientRect.top);
		_Wndw->_Hwnd = CreateWindowExW(
			0L,
			wcex.lpszClassName,
			windowTitle.c_str(),
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			0,
			clientRect.right - clientRect.left,
			clientRect.bottom - clientRect.top,
			NULL,
			NULL,
			_Wndw->_Hinst,
			static_cast<LPVOID>(this)
			);

		if (_Wndw->_Hwnd == nullptr) {
			throw_get_last_error<no_graphics_2d_support>("Could not create window.");
		}
		if (RegisterTouchWindow(_Wndw->_Hwnd, 0) == FALSE) {
			throw_get_last_error<::std::logic_error>("Failed call to RegisterTouchWindow.");
		}
		ShowWindow(_Wndw->_Hwnd, _Wndw->_CmdShow);
		UpdateWindow(_Wndw->_Hwnd);
}

window::window(::std::int32_t preferredWidth, ::std::int32_t preferredHeight, const ::std::wstring& preferredWindowTitle)
	: _Wndw(new _Wndw_t())
	, _Draw_fn()
	, _Mouse_fn()
	, _Mouse_wheel_fn()
	, _Keyboard_event_fn() {
		_Wndw->_Width = preferredWidth;
		_Wndw->_Height = preferredHeight;
		assert(preferredWidth > 0 && preferredHeight > 0);
		WNDCLASSEX wcex = {};
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = &_Wndw_t::WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = sizeof(window*);
		wcex.hInstance = _Wndw->_Hinst;
		wcex.hIcon = nullptr;
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = nullptr;
		wcex.lpszClassName = preferredWindowTitle.c_str();
		wcex.hIconSm = nullptr;

		ATOM atom = RegisterClassEx(&wcex);

		if (atom == 0) {
			throw_get_last_error<no_graphics_2d_support>("Could not register window class.");
		}

		RECT clientRect = { 0, 0, static_cast<LONG>(preferredWidth), static_cast<LONG>(preferredHeight) };
		if (AdjustWindowRect(&clientRect, WS_OVERLAPPEDWINDOW, FALSE) == FALSE) {
			throw_get_last_error<::std::runtime_error>("GetClientRect call failed on window that was just created.");
		}
		preferredWidth = static_cast<::std::int32_t>(clientRect.right - clientRect.left);
		preferredHeight = static_cast<::std::int32_t>(clientRect.bottom - clientRect.top);
		_Wndw->_Hwnd = CreateWindowExW(
			0L,
			wcex.lpszClassName,
			preferredWindowTitle.c_str(),
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			0,
			clientRect.right - clientRect.left,
			clientRect.bottom - clientRect.top,
			NULL,
			NULL,
			_Wndw->_Hinst,
			static_cast<LPVOID>(this)
			);

		if (_Wndw->_Hwnd == nullptr) {
			throw_get_last_error<no_graphics_2d_support>("Could not create window.");
		}
		if (RegisterTouchWindow(_Wndw->_Hwnd, 0) == FALSE) {
			throw_get_last_error<::std::logic_error>("Failed call to RegisterTouchWindow.");
		}
		ShowWindow(_Wndw->_Hwnd, _Wndw->_CmdShow);
		UpdateWindow(_Wndw->_Hwnd);
}

window::window(window::native_handle_type handle)
	: _Wndw(new _Wndw_t())
	, _Draw_fn()
	, _Mouse_fn()
	, _Mouse_wheel_fn()
	, _Keyboard_event_fn() {
		if (handle == nullptr) {
			throw ::std::invalid_argument("handle cannot be null when creating a window from an existing native handle type.");
		}

		_Wndw->_Hwnd = handle;

		RECT clientRect;
		if (GetClientRect(handle, &clientRect) == FALSE) {
			throw_get_last_error<no_graphics_2d_support>("Could not get client rect for existing window.");
		}
		_Wndw->_Width = static_cast<::std::int32_t>(clientRect.right);
		_Wndw->_Height = static_cast<::std::int32_t>(clientRect.bottom);

		if (RegisterTouchWindow(_Wndw->_Hwnd, 0) == FALSE) {
			throw_get_last_error<::std::logic_error>("Failed call to RegisterTouchWindow.");
		}
}

window::~window() {
}

window::native_handle_type window::native_handle() const {
	return _Wndw->_Hwnd;
}

bool window::is_valid() const {
	return _Wndw->_Hwnd != nullptr;
}

void window::run_event_loop() {
	MSG msg = {};
	while (msg.message != WM_QUIT) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			if (static_cast<bool>(_Draw_fn)) {
				_Draw_fn();
			}
		}
	}
	if (msg.wParam != 0) {
		auto exitCode = static_cast<int>(msg.wParam);
		exit(exitCode);
	}
}

void window::set_draw_function(const ::std::function<void()>& fn) {
	_Draw_fn = fn; 
}

void window::set_draw_function(::std::function<void()>&& fn) {
	_Draw_fn = fn;
}

void window::set_mouse_function(const ::std::function<void(mouse_event_type event_type, mouse_button button, const vector_2& coordinates)>& fn) {
	_Mouse_fn = fn;
}

void window::set_mouse_function(::std::function<void(mouse_event_type event_type, mouse_button button, const vector_2& coordinates)>&& fn) {
	_Mouse_fn = ::std::move(fn);
}

void window::set_mouse_wheel_function(const ::std::function<void(mouse_event_type event_type, mouse_button button, ::std::int32_t value)>& fn) {
	_Mouse_wheel_fn = fn;
}

void window::set_mouse_wheel_function(::std::function<void(mouse_event_type event_type, mouse_button button, ::std::int32_t value)>&& fn) {
	_Mouse_wheel_fn = ::std::move(fn);
}

void window::set_keyboard_function(const ::std::function<void(keyboard_event keyEvent)>& fn) {
	_Keyboard_event_fn = fn;
}

void window::set_keyboard_function(::std::function<void(keyboard_event keyEvent)>&& fn) {
	_Keyboard_event_fn = ::std::move(fn);
}

void window::call_mouse_function(mouse_event_type eventType, mouse_button button, const vector_2& coordinates) {
	if (static_cast<bool>(_Mouse_fn)) {
		_Mouse_fn(eventType, button, coordinates);
	}
}

void window::call_mouse_wheel_function(mouse_event_type eventType, mouse_button button, ::std::int32_t value) {
	if (static_cast<bool>(_Mouse_wheel_fn)) {
		_Mouse_wheel_fn(eventType, button, value);
	}
}

void window::call_keyboard_function(keyboard_event keyEvent) {
	if (static_cast<bool>(_Keyboard_event_fn)) {
		_Keyboard_event_fn(keyEvent);
	}
}

void window::draw() {
	if (static_cast<bool>(_Draw_fn)) {
		_Draw_fn();
	}
}

::std::int32_t window::get_width() const {
	return _Wndw->_Width;
}

::std::int32_t window::get_height() const {
	return _Wndw->_Height;
}

mouse_button get_mouse_button_for_wparam(WPARAM wParam) {
	mouse_button button = mouse_button::unknown;
	if (wParam & MK_LBUTTON) {
		button |= mouse_button::left;
	}
	if (wParam & MK_MBUTTON) {
		button |= mouse_button::middle;
	}
	if (wParam & MK_RBUTTON) {
		button |= mouse_button::right;
	}
	if (wParam & MK_XBUTTON1) {
		button |= mouse_button::x1;
	}
	if (wParam & MK_XBUTTON2) {
		button |= mouse_button::x2;
	}
	return button;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPTSTR lpCmdLine, _In_ int nCmdShow) {
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	_Wndw_t::_Hinst = hInstance;
	_Wndw_t::_CmdShow = nCmdShow;

	// Note: This violates the C++ standard. When implemented by VC++, the Microsoft CRT could be adjusted accordingly to
	// remove the need for this.
	return main(__argc, __argv);
}

LRESULT CALLBACK _Wndw_impl_t::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	static bool hasFocus = false;

	static bool validTouchId = false;
	static DWORD touchId = 0;

	if (message == WM_NCCREATE) {
		auto createStruct = reinterpret_cast<CREATESTRUCT*>(lParam);
		auto windowPtr = reinterpret_cast<LONG_PTR>(createStruct->lpCreateParams);
		SetWindowLongPtr(hWnd, 0, windowPtr);
	}

	auto wndw = reinterpret_cast<window*>(GetWindowLongPtr(hWnd, 0));

	switch (message) {
	case WM_DESTROY:
		if (hasFocus) {
			hasFocus = false;
			ReleaseCapture();
			SetFocus(nullptr);
		}
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		{ // Create scope.
			keyboard_event keyEvent = {};
			switch (wParam)	{
			case VK_LEFT:
				keyEvent.key = keyboard_key::left;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_RIGHT:
				keyEvent.key = keyboard_key::right;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_UP:
				keyEvent.key = keyboard_key::up;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_DOWN:
				keyEvent.key = keyboard_key::down;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_HOME:
				keyEvent.key = keyboard_key::home;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_END:
				keyEvent.key = keyboard_key::end;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_INSERT:
				keyEvent.key = keyboard_key::insert;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_DELETE:
				keyEvent.key = keyboard_key::delete_key;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_PRIOR:
				keyEvent.key = keyboard_key::page_up;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_NEXT:
				keyEvent.key = keyboard_key::page_down;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_NUMLOCK:
				keyEvent.key = keyboard_key::num_lock;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_CAPITAL:
				keyEvent.key = keyboard_key::caps_lock;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_SHIFT:
				keyEvent.key = keyboard_key::shift;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_RSHIFT:
				keyEvent.key = keyboard_key::shift;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_CONTROL:
				keyEvent.key = keyboard_key::control;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_RCONTROL:
				keyEvent.key = keyboard_key::control;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_LWIN:
				keyEvent.key = keyboard_key::platform_key;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_RWIN:
				keyEvent.key = keyboard_key::platform_key;
				wndw->call_keyboard_function(keyEvent);
				break;

			case VK_F1:
				keyEvent.key = keyboard_key::f1;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F2:
				keyEvent.key = keyboard_key::f2;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F3:
				keyEvent.key = keyboard_key::f3;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F4:
				keyEvent.key = keyboard_key::f4;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F5:
				keyEvent.key = keyboard_key::f5;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F6:
				keyEvent.key = keyboard_key::f6;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F7:
				keyEvent.key = keyboard_key::f7;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F8:
				keyEvent.key = keyboard_key::f8;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F9:
				keyEvent.key = keyboard_key::f9;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F10:
				keyEvent.key = keyboard_key::f10;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F11:
				keyEvent.key = keyboard_key::f11;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F12:
				keyEvent.key = keyboard_key::f12;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F13:
				keyEvent.key = keyboard_key::f13;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F14:
				keyEvent.key = keyboard_key::f14;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F15:
				keyEvent.key = keyboard_key::f15;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F16:
				keyEvent.key = keyboard_key::f16;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F17:
				keyEvent.key = keyboard_key::f17;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F18:
				keyEvent.key = keyboard_key::f18;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F19:
				keyEvent.key = keyboard_key::f19;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F20:
				keyEvent.key = keyboard_key::f20;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F21:
				keyEvent.key = keyboard_key::f21;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F22:
				keyEvent.key = keyboard_key::f22;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F23:
				keyEvent.key = keyboard_key::f23;
				wndw->call_keyboard_function(keyEvent);
				break;
			case VK_F24:
				keyEvent.key = keyboard_key::f24;
				wndw->call_keyboard_function(keyEvent);
				break;
			default:
				//OutputDebugString(std::to_wstring(wParam).append(L"\n").c_str());
				//assert(false && "Unknown extended key.");
				//keyEvent.key = keyboard_key::unknown;
				//wndw->call_keyboard_function(keyEvent);
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
		break;
	case WM_CHAR:
		{
			keyboard_event keyEvent = {};
			switch (wParam)
			{
			case 0x08: // backspace
				keyEvent.key = keyboard_key::backspace;
				wndw->call_keyboard_function(keyEvent);
				break;
			case 0x09: // tab
				keyEvent.key = keyboard_key::tab;
				wndw->call_keyboard_function(keyEvent);
				break;
			case 0x0A: // linefeed
				keyEvent.key = keyboard_key::enter;
				wndw->call_keyboard_function(keyEvent);
				break;
			case 0x0D: // carriage-return
				keyEvent.key = keyboard_key::enter;
				wndw->call_keyboard_function(keyEvent);
				break;
			case 0x1B: // escape
				keyEvent.key = keyboard_key::escape;
				wndw->call_keyboard_function(keyEvent);
				break;
			default:
				keyEvent.key = keyboard_key::character_value;
				keyEvent.character = static_cast<wchar_t>(wParam);
				wndw->call_keyboard_function(keyEvent);
				break;
			}
		}
		break;

	case WM_CAPTURECHANGED:
		if (reinterpret_cast<HWND>(lParam) != hWnd) {
			hasFocus = false;
		}
		break;
	case WM_TOUCH:
		{ // Create scope.
			auto closeTouchHandle = guarantee_action([&lParam]() { CloseTouchInputHandle(reinterpret_cast<HTOUCHINPUT>(lParam)); });
			::std::vector<TOUCHINPUT> touchInputPoints;
			const UINT touchCount = static_cast<UINT>(LOWORD(wParam));
			touchInputPoints.resize(touchCount);
			float x = 0;
			float y = 0;
			if (GetTouchInputInfo(reinterpret_cast<HTOUCHINPUT>(lParam), touchCount, &(touchInputPoints[0]), sizeof(TOUCHINPUT)) != FALSE) {
				if (validTouchId) {
					bool foundId = false;
					for (UINT i = 0; i < touchCount; ++i) {
						if (touchInputPoints[i].dwID == touchId) {
							foundId = true;
							const auto& input = touchInputPoints[i];
							if (input.dwFlags & TOUCHEVENTF_UP) {
								validTouchId = false;
							}
							else {
								x = static_cast<float>(touchInputPoints[i].x / 100);
								y = static_cast<float>(touchInputPoints[i].y / 100);
								RECT screenPosWindow;
								if (GetWindowRect(hWnd, &screenPosWindow) == FALSE) {
									throw_get_last_error<::std::logic_error>("Failed call to GetWindowRect.");
								}
								x -= screenPosWindow.left;
								y -= screenPosWindow.top;
								wndw->call_mouse_function(mouse_event_type::move, mouse_button::left, vector_2(x, y));
							}
							break;
						}
					}
					if (!foundId) {
						validTouchId = false;
						wndw->call_mouse_function(mouse_event_type::button_up, mouse_button::left, vector_2(x, y));
					}
				}
				else {
					for (UINT i = 0; i < touchCount; ++i) {
						if (touchInputPoints[i].dwFlags == TOUCHEVENTF_PRIMARY) {
							validTouchId = true;
							touchId = touchInputPoints[i].dwID;
							auto x = static_cast<float>(touchInputPoints[i].x / 100);
							auto y = static_cast<float>(touchInputPoints[i].y / 100);
							RECT screenPosWindow;
							if (GetWindowRect(hWnd, &screenPosWindow) == FALSE) {
								throw_get_last_error<::std::logic_error>("Failed call to GetWindowRect.");
							}
							x -= screenPosWindow.left;
							y -= screenPosWindow.top;
							wndw->call_mouse_function(mouse_event_type::button_down, mouse_button::left, vector_2(x, y));
						}
					}
				}
			}
			else {
				throw_get_last_error<::std::runtime_error>("Failed call to GetTouchInputInfo.");
			}
		}
		break;
	case WM_LBUTTONDOWN:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		hasFocus = true;
		SetCapture(hWnd);
		wndw->call_mouse_function(mouse_event_type::button_down, mouse_button::left, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;
	case WM_MBUTTONDOWN:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		hasFocus = true;
		SetCapture(hWnd);
		wndw->call_mouse_function(mouse_event_type::button_down, mouse_button::middle, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;
	case WM_RBUTTONDOWN:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		hasFocus = true;
		SetCapture(hWnd);
		wndw->call_mouse_function(mouse_event_type::button_down, mouse_button::right, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;
	case WM_XBUTTONDOWN:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		switch (GET_XBUTTON_WPARAM(wParam)) {
		case XBUTTON1:
			hasFocus = true;
			SetCapture(hWnd);
			wndw->call_mouse_function(mouse_event_type::button_down, mouse_button::x1, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
			break;
		case XBUTTON2:
			hasFocus = true;
			SetCapture(hWnd);
			wndw->call_mouse_function(mouse_event_type::button_down, mouse_button::x2, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
			break;
		default:
			hasFocus = true;
			SetCapture(hWnd);
			wndw->call_mouse_function(mouse_event_type::button_down, mouse_button::unknown, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
			break;
		}
		break;
	case WM_NCLBUTTONUP:
		// Intentional fall-through.
	case WM_LBUTTONUP:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		hasFocus = false;
		ReleaseCapture();
		wndw->call_mouse_function(mouse_event_type::button_up, mouse_button::left, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;
	case WM_NCMBUTTONUP:
		// Intentional fall-through.
	case WM_MBUTTONUP:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		hasFocus = false;
		ReleaseCapture();
		wndw->call_mouse_function(mouse_event_type::button_up, mouse_button::middle, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;
	case WM_NCRBUTTONUP:
		// Intentional fall-through.
	case WM_RBUTTONUP:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		hasFocus = false;
		ReleaseCapture();
		wndw->call_mouse_function(mouse_event_type::button_up, mouse_button::right, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;
	case WM_NCXBUTTONUP:
		// Intentional fall-through.
	case WM_XBUTTONUP:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		switch (GET_XBUTTON_WPARAM(wParam)) {
		case XBUTTON1:
			hasFocus = false;
			ReleaseCapture();
			wndw->call_mouse_function(mouse_event_type::button_up, mouse_button::x1, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
			break;
		case XBUTTON2:
			hasFocus = false;
			ReleaseCapture();
			wndw->call_mouse_function(mouse_event_type::button_up, mouse_button::x2, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
			break;
		default:
			hasFocus = false;
			ReleaseCapture();
			wndw->call_mouse_function(mouse_event_type::button_up, mouse_button::unknown, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
			break;
		}
		break;

	case WM_LBUTTONDBLCLK:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		wndw->call_mouse_function(mouse_event_type::double_click, mouse_button::left, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;
	case WM_MBUTTONDBLCLK:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		wndw->call_mouse_function(mouse_event_type::double_click, mouse_button::middle, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;
	case WM_RBUTTONDBLCLK:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		wndw->call_mouse_function(mouse_event_type::double_click, mouse_button::right, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;
	case WM_XBUTTONDBLCLK:
		if (wndw == nullptr) {
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		switch (GET_XBUTTON_WPARAM(wParam)) {
		case XBUTTON1:
			wndw->call_mouse_function(mouse_event_type::double_click, mouse_button::x1, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
			break;
		case XBUTTON2:
			wndw->call_mouse_function(mouse_event_type::double_click, mouse_button::x2, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
			break;
		default:
			wndw->call_mouse_function(mouse_event_type::double_click, mouse_button::unknown, vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
			break;
		}
		break;

	case WM_MOUSEMOVE:
		wndw->call_mouse_function(mouse_event_type::move, get_mouse_button_for_wparam(wParam), vector_2(static_cast<float>(GET_X_LPARAM(lParam)), static_cast<float>(GET_Y_LPARAM(lParam))));
		break;

	case WM_MOUSEWHEEL:
		//{
		//	std::wstringstream str;
		//	str << GET_WHEEL_DELTA_WPARAM(wParam) << L"\n";
		//	OutputDebugStringW(str.str().c_str());
		//}
		wndw->call_mouse_wheel_function(mouse_event_type::wheel, get_mouse_button_for_wparam(wParam), GET_WHEEL_DELTA_WPARAM(wParam));
		break;
	case WM_MOUSEHWHEEL:
		wndw->call_mouse_wheel_function(mouse_event_type::horizontal_wheel, get_mouse_button_for_wparam(wParam), GET_WHEEL_DELTA_WPARAM(wParam));
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
