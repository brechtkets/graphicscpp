#include "stdafx.h"

#include "graphics_2d.h"

_D2d_dwrite_path_geometry_impl::native_handle_type _D2d_dwrite_path_geometry_impl::native_handle() const {
	return _PathGeometry.Get();
}

void _D2d_dwrite_path_geometry_impl::begin_define(const ::std_graphics_2d_ts::vector_2& beginPoint) {
	throw_if_failed_hresult<::std::runtime_error>(
		_PathGeometry->Open(&_GeometrySink),
		"Failed call to ID2D1PathGeometry::Open."
		);
	_GeometrySink->BeginFigure(D2D1::Point2F(beginPoint.x, beginPoint.y), D2D1_FIGURE_BEGIN_FILLED);
}

void _D2d_dwrite_path_geometry_impl::end_define() {
	throw_if_null<::std::logic_error>(_GeometrySink.Get(), "You must call begin_define before calling end_define.");
	_GeometrySink->EndFigure(D2D1_FIGURE_END_CLOSED);

	throw_if_failed_hresult<::std::runtime_error>(
		_GeometrySink->Close(),
		"Failed call to ID2D1SimplifiedGeometrySink::Close."
		);

	_GeometrySink.Reset();
}

void _D2d_dwrite_path_geometry_impl::add_line(const ::std_graphics_2d_ts::vector_2& toPoint) {
	throw_if_null<::std::logic_error>(_GeometrySink.Get(), "You must call begin_define before calling end_define.");

	_GeometrySink->AddLine(D2D1::Point2F(toPoint.x, toPoint.y));
}

void _D2d_dwrite_path_geometry_impl::add_bezier(const ::std_graphics_2d_ts::vector_2& toPoint, const ::std_graphics_2d_ts::vector_2& controlPoint1, const ::std_graphics_2d_ts::vector_2& controlPoint2) {
	throw_if_null<::std::logic_error>(_GeometrySink.Get(), "You must call begin_define before calling end_define.");

	_GeometrySink->AddBezier(D2D1::BezierSegment(
		D2D1::Point2F(controlPoint1.x, controlPoint1.y),
		D2D1::Point2F(controlPoint2.x, controlPoint2.y),
		D2D1::Point2F(toPoint.x, toPoint.y)
		));
}
