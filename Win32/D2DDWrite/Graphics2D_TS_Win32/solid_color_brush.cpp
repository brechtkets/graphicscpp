#include "stdafx.h"
#include "graphics_2d.h"

using namespace std_graphics_2d_ts;

_D2d_dwrite_solid_color_brush_impl::native_handle_type _D2d_dwrite_solid_color_brush_impl::native_handle() const {
	return _Brush.Get();
}