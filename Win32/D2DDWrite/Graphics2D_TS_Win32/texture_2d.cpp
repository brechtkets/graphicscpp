#include "stdafx.h"
#include "graphics_2d.h"

using namespace std_graphics_2d_ts;

_D2d_dwrite_texture_2d_impl::native_handle_type _D2d_dwrite_texture_2d_impl::native_handle() const {
	return static_cast<_D2d_dwrite_texture_2d_impl::native_handle_type>(_Texture.Get());
}