#include "stdafx.h"
#include "graphics_2d.h"
#include <sstream>

#pragma comment(lib, "d3d11")
#pragma comment(lib, "dxgi")
#pragma comment(lib, "d2d1")
#pragma comment(lib, "dwrite")

using namespace std_graphics_2d_ts;

_D2d_dwrite_grphcs_dvce_impl::_D2d_dwrite_grphcs_dvce_impl(const window& wndw, const pixel_format& preferredFormat) : _Device_data(new _D2d_dwrite_grphcs_dvce_impl_data) {
	auto preferredWidth = wndw.get_width();
	auto preferredHeight = wndw.get_height();

	const D3D_FEATURE_LEVEL featureLevels[] = {
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1,
	};

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc = {};
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;
	dxgiSwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	dxgiSwapChainDesc.BufferCount = 2;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.BufferDesc.Width = preferredWidth;
	dxgiSwapChainDesc.BufferDesc.Height = preferredHeight;
	dxgiSwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_STRETCHED;
	auto format = preferredFormat.format();
	switch (format)
	{
	case std_graphics_2d_ts::pixel_format::none:
		throw ::std::invalid_argument("preferredFormat.format cannot be pixel_format::none");
		break;
	case std_graphics_2d_ts::pixel_format::native:
		dxgiSwapChainDesc.BufferDesc.Format = preferredFormat.native_format();
		break;
	case std_graphics_2d_ts::pixel_format::r8_g8_b8_a8:
		dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		break;
	case std_graphics_2d_ts::pixel_format::b8_g8_r8_a8:
		dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		break;
	default:
		throw ::std::invalid_argument("preferredFormat: unknown pixel_format_type.");
		break;
	}

	dxgiSwapChainDesc.OutputWindow = wndw.native_handle();

	D3D_FEATURE_LEVEL fl;

	throw_if_failed_hresult<no_graphics_2d_support>(
		D3D11CreateDeviceAndSwapChain(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		D3D11_CREATE_DEVICE_BGRA_SUPPORT,
		featureLevels,
		ARRAYSIZE(featureLevels),
		D3D11_SDK_VERSION,
		&dxgiSwapChainDesc,
		&_Device_data->_SwpChn,
		&_Device_data->_D3DDevice,
		&fl,
		&_Device_data->_D3DContext
		),
		"Error calling D3D11CreateDeviceAndSwapChain."
		);

	Microsoft::WRL::ComPtr<ID3D11Texture2D> bbTexture2D;
	throw_if_failed_hresult<no_graphics_2d_support>(
		_Device_data->_SwpChn->GetBuffer(0, IID_PPV_ARGS(&bbTexture2D)),
		"Error calling IDXGISwapChain::GetBuffer"
		);

	auto rtvDesc = CD3D11_RENDER_TARGET_VIEW_DESC(bbTexture2D.Get(), D3D11_RTV_DIMENSION_TEXTURE2D);

	throw_if_failed_hresult<no_graphics_2d_support>(
		_Device_data->_D3DDevice->CreateRenderTargetView(bbTexture2D.Get(), &rtvDesc, &_Device_data->_D3DRTV),
		"Error calling ID3D11Device::CreateRenderTargetView."
		);

	CD3D11_VIEWPORT viewport(0.0f, 0.0f, static_cast<float>(preferredWidth), static_cast<float>(preferredHeight));

	_Device_data->_D3DContext->RSSetViewports(1, &viewport);

	D2D1_FACTORY_OPTIONS d2d1FactoryOptions = {};
	d2d1FactoryOptions.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;

	throw_if_failed_hresult<no_graphics_2d_support>(
		D2D1CreateFactory(
		D2D1_FACTORY_TYPE_SINGLE_THREADED,
		__uuidof(ID2D1Factory1),
		&d2d1FactoryOptions,
		&_Device_data->_Direct2DFactory
		),
		"Error calling D2D1CreateFactory."
		);

	Microsoft::WRL::ComPtr<IDXGIDevice> dxgiDevice;
	throw_if_failed_hresult<no_graphics_2d_support>(
		_Device_data->_D3DDevice.As(&dxgiDevice),
		"Error getting IDXGIDevice interface from ID3D11Device."
		);

	D2D1_CREATION_PROPERTIES d2d1CreationProps = {};
	d2d1CreationProps.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;

	throw_if_failed_hresult<no_graphics_2d_support>(
		_Device_data->_Direct2DFactory->CreateDevice(
		dxgiDevice.Get(),
		&_Device_data->_Direct2DDevice
		),
		"Error calling 1D2D1Factory1::CreateDevice"
		);

	throw_if_failed_hresult<no_graphics_2d_support>(
		_Device_data->_Direct2DDevice->CreateDeviceContext(
		D2D1_DEVICE_CONTEXT_OPTIONS_NONE,
		&_Device_data->_Direct2DContext
		),
		"Error calling ID2D1Device::CreateDeviceContext."
		);

	Microsoft::WRL::ComPtr<IDXGISurface> dxgiSurface;
	throw_if_failed_hresult<no_graphics_2d_support>(
		_Device_data->_SwpChn->GetBuffer(
		0,
		IID_PPV_ARGS(&dxgiSurface)
		),
		"Error calling IDXGISwapChain::GetBuffer."
		);

	D2D1_BITMAP_PROPERTIES1 d2d1BitmapProperties = {};
	d2d1BitmapProperties.pixelFormat.format = dxgiSwapChainDesc.BufferDesc.Format;
	d2d1BitmapProperties.pixelFormat.alphaMode = D2D1_ALPHA_MODE_PREMULTIPLIED;
	_Device_data->_Direct2DFactory->GetDesktopDpi(&d2d1BitmapProperties.dpiX, &d2d1BitmapProperties.dpiY);
	d2d1BitmapProperties.bitmapOptions = D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW;

	throw_if_failed_hresult<no_graphics_2d_support>(
		_Device_data->_Direct2DContext->CreateBitmapFromDxgiSurface(
		dxgiSurface.Get(),
		d2d1BitmapProperties,
		&_Device_data->_Direct2DSwapChainTargetBitmap
		),
		"Error calling ID2D1DeviceContext::CreateBitmapFromDxgiSurface"
		);

	_Device_data->_Direct2DContext->SetTarget(_Device_data->_Direct2DSwapChainTargetBitmap.Get());

	throw_if_failed_hresult<no_graphics_2d_support>(
		DWriteCreateFactory(
		DWRITE_FACTORY_TYPE_SHARED,
		__uuidof(_Device_data->_DWriteFactory),
		&_Device_data->_DWriteFactory
		),
		"Error calling DWriteCreateFactory."
		);

	_Device_data->_CommonStates = std::make_shared<DirectX::CommonStates>(_Device_data->_D3DDevice.Get());
}

_D2d_dwrite_grphcs_dvce_impl::native_handle_type _D2d_dwrite_grphcs_dvce_impl::native_handle() const {
	return static_cast<_D2d_dwrite_grphcs_dvce_impl::native_handle_type>(_Device_data->_Direct2DDevice.Get());
}

texture_2d<graphics_traits_2d<_D2d_dwrite_graphics>> _D2d_dwrite_grphcs_dvce_impl::load_texture_2d(const char* filename) {
	return load_texture_2d(::std::string(filename));
}

texture_2d<graphics_traits_2d<_D2d_dwrite_graphics>> _D2d_dwrite_grphcs_dvce_impl::load_texture_2d(const wchar_t* filename) {
	return load_texture_2d(::std::wstring(filename));
}

texture_2d<graphics_traits_2d<_D2d_dwrite_graphics>> _D2d_dwrite_grphcs_dvce_impl::load_texture_2d(const ::std::string& filename) {
	return load_texture_2d(_UTF8_to_wstring(filename));
}

texture_2d<graphics_traits_2d<_D2d_dwrite_graphics>> _D2d_dwrite_grphcs_dvce_impl::load_texture_2d(const ::std::wstring& filename) {
	Microsoft::WRL::ComPtr<ID3D11Resource> resource;
	bool isDDS = false;
	if (filename.length() >= 5) {
		auto suffix = filename.substr(filename.length() - 4, 4);
		if (_wcsicmp(L".dds", suffix.c_str()) == 0) {
			isDDS = true;
		}
	}
	if (isDDS) {
		throw_if_failed_hresult<std::logic_error>(
			DirectX::CreateDDSTextureFromFile(_Device_data->_D3DDevice.Get(), filename.c_str(), &resource, nullptr),
			"Failed call to DirectX::CreateDDSTextureFromFile."
			);
	}
	else {
		throw_if_failed_hresult<std::logic_error>(
			DirectX::CreateWICTextureFromFile(_Device_data->_D3DDevice.Get(), nullptr, filename.c_str(), &resource, nullptr),
			"Failed call to DirectX::CreateWICTextureFromFile."
			);
	}
	Microsoft::WRL::ComPtr<IDXGISurface> dxgiSurface;
	resource.As(&dxgiSurface);

	auto texture = std::make_shared<_D2d_dwrite_graphics::texture_2d_type>();

	throw_if_failed_hresult<std::logic_error>(
		_Device_data->_Direct2DContext->CreateBitmapFromDxgiSurface(dxgiSurface.Get(), nullptr, &texture->_Texture),
		"Failed call to ID2D1DeviceContext::CreateBitmapFromDxgiSurface."
		);

	return texture_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>>(texture);
}

font_object<graphics_traits_2d<_D2d_dwrite_graphics>> _D2d_dwrite_grphcs_dvce_impl::create_font_object(const char* fontFamily, float pointSize) {
	return create_font_object(::std::string(fontFamily), pointSize);
}

font_object<graphics_traits_2d<_D2d_dwrite_graphics>> _D2d_dwrite_grphcs_dvce_impl::create_font_object(const wchar_t* fontFamily, float pointSize) {
	return create_font_object(::std::wstring(fontFamily), pointSize);
}

font_object<graphics_traits_2d<_D2d_dwrite_graphics>> _D2d_dwrite_grphcs_dvce_impl::create_font_object(const ::std::string& fontFamily, float pointSize) {
	return create_font_object(_UTF8_to_wstring(fontFamily), pointSize);
}

font_object<graphics_traits_2d<_D2d_dwrite_graphics>> _D2d_dwrite_grphcs_dvce_impl::create_font_object(const ::std::wstring& fontFamily, float pointSize) {
	auto fontSize = (pointSize * 72.0f) / 96.0f;
	_D2d_dwrite_graphics::font_object_type fontObj;

	throw_if_failed_hresult<std::logic_error>(
		_Device_data->_DWriteFactory->CreateTextFormat(
		fontFamily.c_str(),
		nullptr,
		DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		fontSize,
		L"",
		&fontObj._TextFormat),
		"Failed call to IDWriteFactory::CreateTextFormat."
		);

	return _D2d_dwrite_graphics::font_object(std::move(fontObj));
}

_D2d_dwrite_graphics::solid_color_brush _D2d_dwrite_grphcs_dvce_impl::create_solid_color_brush(const rgba_unorm_color& color) {
	std::shared_ptr<_D2d_dwrite_graphics::solid_color_brush_type> solidColorBrush(new _D2d_dwrite_graphics::solid_color_brush_type);
	throw_if_failed_hresult<::std::logic_error>(
		_Device_data->_Direct2DContext->CreateSolidColorBrush(
		D2D1::ColorF(color.r, color.g, color.b, color.a),
		&solidColorBrush->_Brush),
		"Failed call to ID2D1DeviceContext::CreateSolidColorBrush"
		);
	return _D2d_dwrite_graphics::solid_color_brush(solidColorBrush);
}

_D2d_dwrite_graphics::path_geometry _D2d_dwrite_grphcs_dvce_impl::create_path_geometry() {
	_D2d_dwrite_path_geometry_impl pathGeometry;
	throw_if_failed_hresult<::std::logic_error>(
		_Device_data->_Direct2DFactory->CreatePathGeometry(&pathGeometry._PathGeometry),
		"Failed call to ID2D1Factory::CreatePathGeometry"
		);
	return _D2d_dwrite_graphics::path_geometry(std::move(pathGeometry));
}

::std::shared_ptr<_D2d_dwrite_grphcs_dvce_impl_data> _D2d_dwrite_grphcs_dvce_impl::get_device_data() const {
	return ::std::shared_ptr<_D2d_dwrite_grphcs_dvce_impl_data>(_Device_data);
}

