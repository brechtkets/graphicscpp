#pragma once
#ifndef _XGRAPHICS_2D_H
#define _XGRAPHICS_2D_H

#include <sstream>
#include "graphics_traits_2d.h"
#include "graphics_2d.h"

#include <wrl.h>
#include <d3d11.h>
#include <d2d1_1.h>
#include <dwrite.h>
#include <dxgi.h>
#include "../DirectXTK/Inc/CommonStates.h"
#include "../DirectXTK/Inc/DDSTextureLoader.h"
#include "../DirectXTK/Inc/WICTextureLoader.h"

namespace std_graphics_2d_ts {
	class pixel_format;
	class no_graphics_2d_support;
	class window;

	template <class GraphicsTraits>
	class graphics_device_2d;

	template <class GraphicsTraits>
	class graphics_context_2d;

	struct rgba_unorm_color;
	struct vector_2;
	struct rect;
	template <class GraphicsTraits>
	class texture_2d;

	template <class GraphicsTraits>
	class font_object;

	template <class GraphicsTraits, class BrushType>
	class brush;

	template <class GraphicsTratis>
	class path_geometry;
}

inline ::std::wstring _UTF8_to_wstring(const ::std::string& utf8Str) {
	int chSizeNeeded = MultiByteToWideChar(CP_UTF8, 0, utf8Str.c_str(), -1, nullptr, 0);
	if (chSizeNeeded == 0) {
		throw_get_last_error<::std::runtime_error>("Could not convert string from UTF-8 to UTF-16.");
	}
	::std::wstring str;
	str.resize(chSizeNeeded);
	if (MultiByteToWideChar(CP_UTF8, 0, utf8Str.c_str(), -1, &str[0], chSizeNeeded) == 0) {
		throw_get_last_error<::std::runtime_error>("Could not convert string from UTF-8 to UTF-16.");
	}
	return str;
}

struct _D2d_dwrite_graphics;

struct _D2d_dwrite_solid_color_brush_impl {
	typedef ID2D1SolidColorBrush* native_handle_type;
	Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> _Brush;

	native_handle_type native_handle() const;
};

//struct _D2d_dwrite_linear_gradient_brush_impl {
//	typedef ID2D1LinearGradientBrush* native_handle_type;
//	Microsoft::WRL::ComPtr<ID2D1LinearGradientBrush> _Brush;
//
//	native_handle_type native_handle() const;
//};

struct _Wndw_impl_t {
	typedef HWND native_handle_type;
	native_handle_type _Hwnd;
	::std::int32_t _Width;
	::std::int32_t _Height;

	static HINSTANCE _Hinst;
	static int _CmdShow;

	_Wndw_impl_t()
		: _Hwnd()
		, _Width()
		, _Height() {
	}

	_Wndw_impl_t(const _Wndw_impl_t& other)
		: _Hwnd (other._Hwnd)
		, _Width(other._Width)
		, _Height(other._Height) {
	}

	_Wndw_impl_t& operator=(const _Wndw_impl_t& other) {
		_Hwnd = other._Hwnd;
		_Width = other._Width;
		_Height = other._Height;
		return *this;
	}

	_Wndw_impl_t(_Wndw_impl_t&& other)
		: _Hwnd(::std::move(other._Hwnd))
		, _Width(::std::move(other._Width))
		, _Height(::std::move(other._Height)) {
	}

	_Wndw_impl_t& operator=(_Wndw_impl_t&& other) {
		if (&other != this) {
			_Hwnd = ::std::move(other._Hwnd);
			_Width = ::std::move(other._Width);
			_Height = ::std::move(other._Height);
		}
		return *this;
	}

	~_Wndw_impl_t() {
	}

	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
};

typedef _Wndw_impl_t _Wndw_t;

class _D2d_dwrite_grphcs_dvce_impl;
class _D2d_dwrite_grphcs_ctxt_impl;
struct _D2d_dwrite_texture_2d_impl;
struct _D2d_dwrite_font_object_impl;
struct _D2d_dwrite_solid_color_brush_impl;
struct _D2d_dwrite_path_geometry_impl;

struct _D2d_dwrite_graphics {
	typedef _D2d_dwrite_graphics traits_type;

	typedef _D2d_dwrite_grphcs_dvce_impl device_type;
	typedef _D2d_dwrite_grphcs_ctxt_impl context_type;
	typedef _D2d_dwrite_texture_2d_impl texture_2d_type;
	typedef _D2d_dwrite_font_object_impl font_object_type;
	typedef _D2d_dwrite_solid_color_brush_impl solid_color_brush_type;
	typedef _D2d_dwrite_path_geometry_impl path_geometry_type;

	typedef std_graphics_2d_ts::graphics_device_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> device;
	typedef std_graphics_2d_ts::graphics_context_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> context;
	typedef std_graphics_2d_ts::texture_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> texture_2d;
	typedef std_graphics_2d_ts::font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> font_object;
	typedef std_graphics_2d_ts::brush<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>, _D2d_dwrite_solid_color_brush_impl> solid_color_brush;
	typedef std_graphics_2d_ts::path_geometry<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> path_geometry;

	bool has_a_display_surface() const {
		return true;
	}
};

struct _D2d_dwrite_grphcs_dvce_impl_data {
	Microsoft::WRL::ComPtr<ID3D11Device>			_D3DDevice;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext>		_D3DContext;
	Microsoft::WRL::ComPtr<IDXGISwapChain>			_SwpChn;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView>	_D3DRTV;
	Microsoft::WRL::ComPtr<ID2D1Factory1>			_Direct2DFactory;
	Microsoft::WRL::ComPtr<ID2D1Device>				_Direct2DDevice;
	Microsoft::WRL::ComPtr<ID2D1DeviceContext>		_Direct2DContext;
	Microsoft::WRL::ComPtr<ID2D1Bitmap1>			_Direct2DSwapChainTargetBitmap;
	Microsoft::WRL::ComPtr<IDWriteFactory>			_DWriteFactory;

	::std::shared_ptr<DirectX::CommonStates>		_CommonStates;
};

class _D2d_dwrite_grphcs_dvce_impl {
	::std::shared_ptr<_D2d_dwrite_grphcs_dvce_impl_data> _Device_data;

public:
	_D2d_dwrite_grphcs_dvce_impl(const std_graphics_2d_ts::window& wndw, const std_graphics_2d_ts::pixel_format& preferredFormat);

	typedef ID2D1Device* native_handle_type;

	native_handle_type native_handle() const;

	std_graphics_2d_ts::texture_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> load_texture_2d(const char* filename);
	std_graphics_2d_ts::texture_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> load_texture_2d(const wchar_t* filename);
	std_graphics_2d_ts::texture_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> load_texture_2d(const ::std::string& filename);
	std_graphics_2d_ts::texture_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> load_texture_2d(const ::std::wstring& filename);

	std_graphics_2d_ts::font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> create_font_object(const char* fontname, float pointSize);
	std_graphics_2d_ts::font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> create_font_object(const wchar_t* fontname, float pointSize);
	std_graphics_2d_ts::font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> create_font_object(const ::std::string& fontname, float pointSize);
	std_graphics_2d_ts::font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> create_font_object(const ::std::wstring& fontname, float pointSize);

	std_graphics_2d_ts::brush<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>, std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush_type> create_solid_color_brush(const std_graphics_2d_ts::rgba_unorm_color& color);

	std_graphics_2d_ts::path_geometry<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> create_path_geometry();

	// Internal implementation details functions

	::std::shared_ptr<_D2d_dwrite_grphcs_dvce_impl_data> get_device_data() const;
};

class _D2d_dwrite_grphcs_ctxt_impl {
	// Variables.

	::std::shared_ptr<_D2d_dwrite_grphcs_dvce_impl_data> _Device_data;

	// Functions.

public:
	typedef ID2D1DeviceContext * native_handle_type;

	explicit _D2d_dwrite_grphcs_ctxt_impl(const _D2d_dwrite_grphcs_dvce_impl& dvce);

	native_handle_type native_handle() const;

	void begin_drawing();

	void end_drawing();

	void clear_drawing_surface(const ::std_graphics_2d_ts::rgba_unorm_color& color);

	void draw_texture_2d(const ::std_graphics_2d_ts::texture_2d<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> &texture, const ::std_graphics_2d_ts::vector_2& dest);

	void draw_string(const ::std_graphics_2d_ts::font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> &font, const char* str, const ::std_graphics_2d_ts::rect& dest, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush);
	void draw_string(const ::std_graphics_2d_ts::font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> &font, const wchar_t* str, const ::std_graphics_2d_ts::rect& dest, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush);
	void draw_string(const ::std_graphics_2d_ts::font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> &font, const ::std::string& str, const ::std_graphics_2d_ts::rect& dest, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush);
	void draw_string(const ::std_graphics_2d_ts::font_object<std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>> &font, const ::std::wstring& str, const ::std_graphics_2d_ts::rect& dest, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush);

	void draw_line(const ::std_graphics_2d_ts::vector_2& start, const ::std_graphics_2d_ts::vector_2& end, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush, float strokeWidth);

	void draw_rect(const ::std_graphics_2d_ts::rect& rect, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush, float strokeWidth);

	void fill_rect(const ::std_graphics_2d_ts::rect& rect, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush);

	void draw_ellipse(const ::std_graphics_2d_ts::vector_2& center, float xRadius, float yRadius, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush, float strokeWidth);

	void fill_ellipse(const ::std_graphics_2d_ts::vector_2& center, float xRadius, float yRadius, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush);

	void draw_path_geometry(const ::std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::path_geometry& pathGeometry, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush, float strokeWidth);

	void fill_path_geometry(const ::std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::path_geometry& pathGeometry, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush);

	void draw_bezier(const ::std_graphics_2d_ts::vector_2& fromPoint, const ::std_graphics_2d_ts::vector_2& toPoint, const ::std_graphics_2d_ts::vector_2& controlPoint1, const ::std_graphics_2d_ts::vector_2& controlPoint2, const std_graphics_2d_ts::graphics_traits_2d<_D2d_dwrite_graphics>::solid_color_brush& brush, float strokeWidth);
};

struct _D2d_dwrite_texture_2d_impl {
	// Variables.

	Microsoft::WRL::ComPtr<ID2D1Bitmap1> _Texture;

	// Functions.

	typedef void * native_handle_type;

	native_handle_type native_handle() const;
};

struct _D2d_dwrite_font_object_impl {
	// Variables.

	Microsoft::WRL::ComPtr<IDWriteTextFormat> _TextFormat;

	// Functions.

	typedef void * native_handle_type;

	native_handle_type native_handle() const;
};

struct _D2d_dwrite_path_geometry_impl {
	// Variables.
	Microsoft::WRL::ComPtr<ID2D1PathGeometry> _PathGeometry;

	Microsoft::WRL::ComPtr<ID2D1GeometrySink> _GeometrySink;

	// Functions.
	typedef ID2D1PathGeometry * native_handle_type;

	native_handle_type native_handle() const;

	void begin_define(const ::std_graphics_2d_ts::vector_2& beginPoint);

	void end_define();

	void add_line(const ::std_graphics_2d_ts::vector_2& toPoint);

	void add_bezier(
		const ::std_graphics_2d_ts::vector_2& toPoint,
		const ::std_graphics_2d_ts::vector_2& controlPoint1,
		const ::std_graphics_2d_ts::vector_2& controlPoint2
		);
};

#endif
